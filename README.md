Zork-ish

Zork-ish is a text based adventure game based on the ever popular Zork 
by Infocom. In this game, you type in commands and the computer will interpret
and carry out those actions, and will print out the result.


How to Play
- To play, simply type in commands as though you were talking to the computer, 
like “Pick up key” or “go north” or even “Attack the scary monster with the 
magic sword.” (don't worry, there are no scary monsters in this game...
or are there?) The computer will be able to interpret these phrases into 
actions and act accordingly.
- To ensure that you are understood, always start your sentences with a verb, 
and always refer to an object.
- When referring to movement, always use the cardinal directions 
(north, south, east, and west)
- Zork-ish assumes that everything in a room can be reached without having to 
move around a room. As such, specifying to the computer to move somewhere is 
only for moving between rooms. To move, simply specify which cardinal direction 
you want to go (think of it almost like a coordinate grid).
- By typing “i” or “inv”, you can view your inventory.
- You also move by just typing the direction or the first letter of the 
direction, such as “north” or “n” to go north.


Commands to get you started:
- Get _____
- Look _____
- Use _____
- Move _____

For Fun
Once you’ve gotten the hang of it, try these commands out: 
- Pickup door
- Use mirror (in the mirror room)